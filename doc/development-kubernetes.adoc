[IMPORTANT]
====
This environment is most useful for checking Kubernetes integration of the application, and you plan to have
your environment as similar to the target environment as this is even possible.
====

If you do not have K3d cluster created already please use this command first:

[source,shell]
----
k3d cluster create bookCluster
----

You can always stop this cluster to save resources with:

[source,shell]
----
k3d cluster stop bookCluster
----

And start it again with:

[source,shell]
----
k3d cluster start bookCluster
----

Now you need to have also your helm repository registered

[source,shell]
----
helm repo add ex-book https://gitlab.com/api/v4/projects/39005882/packages/helm/stable
----

After registering our example public helm repository we just need to update our helm repository list

[source,shell]
----
helm repo update
----

Now you can use prepared `deploy-k3s.sh` script:

[source,shell]
----
./deploy-k3s.sh
----

which looks like this:

[source,shell]
----
include::../deploy-k3s.sh[]
----
