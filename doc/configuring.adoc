[WARNING]
====
For the simplicity in this project we will violate of
twelve-factorfootnote:[The Twelve-Factor App - Config https://12factor.net/config]
*strict separation of config from code*,
and we will be storing all configuration in the `local-vault.yml` file which have secrets stored as
vaultfootnote:[Encrypting content with ansible vault https://docs.ansible.com/ansible/latest/user_guide/vault.html#encrypting-content-with-ansible-vault] encrypted strings.

Please remember to or use ENVIRONMENT VARIABLES or to use tools like Vaultfootnote:[Vault https://www.vaultproject.io/] in
the production ready application.
====

In this project our configuration for the project for all of it environments is managed with a help of Ansible.
Please just use prepared shell script `local-configure.sh` which look like this:

[source,bash]
----
include::../local-configure.sh[]
----

Because this is only for the presentation purposes password used to encrypt `local-vault.yml` file
is: `ThisIsExamplePassword4U`

By the end of running this you should have files in the `secret` folder which all required configuration
files.

Ansible role used for this project is located in the https://gitlab.com/mobica-workshops/mobica-workshops/examples/ansible/roles/js/vuejs/secrets.book-frontend[secrets.book-frontend] repository.
In this repository we have helm values file https://gitlab.com/mobica-workshops/examples/ansible/roles/go/gorilla/secrets.book-frontend/-/blob/master/templates/helm-values.yaml.j2[templates/helm-values.yaml] which is used to generate our environment specific values files.